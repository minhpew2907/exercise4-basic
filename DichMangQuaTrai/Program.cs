﻿
int[] numbers = { 1, 2, 3, 4, 5, 6 };

int temporaryVariable = numbers[0];

Console.Write("Phan tu truoc khi dich qua ben trai: ");
foreach (int number in numbers)
{
    Console.Write(number + " ");
}

Console.WriteLine();

for (int i = 0; i < numbers.Length - 1; i++)
{
    numbers[i] = numbers[i + 1];
}

numbers[numbers.Length - 1] = temporaryVariable;

Console.Write("\nMang sau khi dich qua ben trai: ");
foreach (int number in numbers)
{
    Console.Write(number + " ");
}

Console.WriteLine();