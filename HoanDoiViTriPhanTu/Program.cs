﻿int[] numbers = { 10, 20, 30, 40, 50 };

Console.Write("Mang ban dau: ");
foreach (var item in numbers)
{
    Console.Write(item + " ");
}

int arrayLength = numbers.Length;
for ( int i = 0; i < arrayLength / 2; i++)
{
    int headNumber = numbers[i];
    numbers[i] = numbers[arrayLength - 1 - i];
    numbers[arrayLength - 1 - i] = headNumber;
}

Console.Write("\nMang sau khi dao nguoc: ");
foreach (var item in numbers)
{
    Console.Write(item + " ");
}




