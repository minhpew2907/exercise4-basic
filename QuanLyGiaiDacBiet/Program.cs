﻿
int[] soXoKienThiet = new int[6];

int soLuongSo = 0;

int soMoi;

int choice = 0;

do
{
    Console.WriteLine("-----Xo so kien thiet-----");
    Console.WriteLine("1. Xem danh sach cac so da them");
    Console.WriteLine("2. Them so moi");
    Console.WriteLine("3. Thoat");
    Console.WriteLine("Nhap lua chon: ");

    choice = int.Parse(Console.ReadLine());

    switch (choice)
    {
        case 1:
            Console.WriteLine("Danh sach cac so duoc luu: ");
            for (int i = 0; i < soLuongSo; i++)
            {
                Console.WriteLine($"{soXoKienThiet[i]}");
            }
            Console.WriteLine();
            break;
        case 2:
            if (soLuongSo < soXoKienThiet.Length)
            {
                Console.WriteLine("Nhap so cua ban: ");
                soMoi = int.Parse(Console.ReadLine());
                soXoKienThiet[soLuongSo] = soMoi;
                soLuongSo++;
                Console.WriteLine($"So {soMoi} da duoc them vao danh sach");
            }
            else
            {
                Console.WriteLine("Danh sach da day, ban khong the them so moi");
            }
            break;
        case 3:
            Console.WriteLine("Cam on quy khach da su dung dich vu");
            Environment.Exit(0);
            break;
        default:
            Console.WriteLine("Chon so trong bang tren");
            break;
    }
} while (choice != 3);